<?php

namespace App\Storage;

/**
 * Класс для работы с крупными JSON фалами.
 */
class StreamJsonStorage implements StorageInterface
{
    /**
     * @var resource
     */
    private $handle;

    /**
     * StreamJsonStorage constructor.
     *
     * @param string $path путь к файлу
     * @param string $mode режим
     * @see fopen
     */
    public function __construct(string $path, string $mode)
    {
        $this->handle = fopen($path, $mode);
        if ($this->handle === false) {
            throw new \RuntimeException('Unable to open file');
        }
    }

    /**
     * @{inheritDoc}
     */
    public function append(string $row): void
    {
        if (flock($this->handle, LOCK_EX) === false) {
            throw new \RuntimeException('Unable to lock handle');
        }
        $firstByte = fread($this->handle, 1);
        if ($firstByte != '[') {
            fseek($this->handle, 0);
            fwrite($this->handle, "[\n");
        }

        // last bytes
        fseek($this->handle, -1, SEEK_END);
        $lastByte = fread($this->handle, 1);
        if ($lastByte == ']') {
            fseek($this->handle, -2, SEEK_END);
            fwrite($this->handle, ",\n");
        }

        // seek to last line
        fseek($this->handle, 0, SEEK_END);
        fwrite($this->handle, $row."\n]");
        flock($this->handle, LOCK_UN);
    }

    /**
     * @{inheritDoc}
     */
    public function find(\Closure $criteria)
    {
        if (flock($this->handle, LOCK_SH) === false) {
            throw new \RuntimeException('Unable to lock handle');
        }
        rewind($this->handle);
        while (!feof($this->handle)) {
            $line = fgets($this->handle);
            if (substr($line, 0, 1) != '{') {
                continue;
            }
            if ($foundResult = $criteria->call($this, trim($line, ",\n"))) {
                return $foundResult;
            }
        }
        flock($this->handle, LOCK_UN);
        return false;
    }
}
