<?php

namespace App\Storage;

/**
 * Интерфейс хранилища данных
 */
interface StorageInterface
{
    /**
     * Добавить строку в хранилище
     *
     * @param string $row
     */
    public function append(string $row): void;

    /**
     * Найти строку в хранилише по кретерию.
     *
     * @param \Closure $criteria критерий поиска, возвращает false
     *                           в случае, если строка не подходит
     *
     * @return string
     */
    public function find(\Closure $criteria);
}
