<?php

namespace App\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use App\Serializer\SerializerInterface;
use App\Storage\StreamJsonStorage;
use App\Entity\AuthInfo;
use Psr\Log\LoggerInterface;

/**
 * Поиск профиля пользователя по nickname + password
 */
class UserProfile implements ConsumerInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var StorageInterface
     */
    protected $storage;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * UserProfile constructor.
     *
     * @param string $filePath путь к файлу
     * @param SerializerInterface $serializer сериализатор
     */
    public function __construct(string $filePath, SerializerInterface $serializer, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->serializer = $serializer;
        $this->storage = new StreamJsonStorage($filePath, 'r');
    }

    /**
     * Найти профиль пользователя по полям nickname и password
     *
     * @param AMQPMessage $message
     * @return mixed
     */
    public function execute(AMQPMessage $message)
    {
        $requestedAuth = $this->serializer->deserialize(
            $message->getBody(),
            AuthInfo::class
        );

        $serializer = $this->serializer;
        $foundData = $this->storage->find(
            function ($line) use ($requestedAuth, $serializer) {
                $user = $serializer->deserialize(
                    $line,
                    \App\Entity\AuthInfo::class
                );
                if ($requestedAuth->getNickname() == $user->getNickname()
                    && $requestedAuth->getPassword() == $user->getPassword()
                ) {
                    return $line;
                }
                return false;
            }
        );
        return $foundData;
    }
}
