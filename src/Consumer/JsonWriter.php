<?php

namespace App\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use App\Storage\{StorageInterface, StreamJsonStorage};

/**
 * Сохраняет JSON сообщений в файл.
 */
abstract class JsonWriter implements ConsumerInterface
{
    /**
     * @var StorageInterface
     */
    protected $storage;

    /**
     * @param \string $filePath путь к файлу
     */
    public function __construct(string $filePath)
    {
        $this->storage = new StreamJsonStorage($filePath, 'r+');
    }

    /**
     * Добавляем записи в файл.
     *
     * @param AMQPMessage $message
     * @return mixed
     */
    public function execute(AMQPMessage $message)
    {
        try {
            $this->storage->append($message->getBody());
        } catch (\RuntimeException $e) {
            return 1;
        }
        return true;
    }
}
