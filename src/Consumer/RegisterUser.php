<?php

namespace App\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use App\Security\IdentityInterface;
use App\Storage\{StorageInterface, StreamJsonStorage};
use App\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Log\Logger;

/**
 * Регистрация пользователей.
 */
class RegisterUser implements ConsumerInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var StorageInterface
     */
    protected $storage;

    /**
     * @var IdentityInterface
     */
    protected $ident;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * UserProfile constructor.
     *
     * @param string $filePath путь к файлу
     * @param SerializerInterface $serializer сериализатор
     */
    public function __construct(
        string $filePath,
        SerializerInterface $serializer,
        IdentityInterface $ident,
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
        $this->ident = $ident;
        $this->serializer = $serializer;
        $this->storage = new StreamJsonStorage($filePath, 'r+');
    }

    /**
     * Найти профиль пользователя по полям nickname и password
     *
     * @param AMQPMessage $message
     * @return mixed
     */
    public function execute(AMQPMessage $message)
    {
        $requestedUser = $this->serializer->deserialize(
            $message->getBody(),
            \App\Entity\User::class
        );

        // находим существующего пользователя по nickname
        $serializer = $this->serializer;
        $logger = $this->logger;
        $foundUser = $this->storage->find(
            function ($line) use ($requestedUser, $serializer, $logger) {
                try {
                    $currentUser = $serializer->deserialize(
                        $line,
                        \App\Entity\User::class
                    );
                } catch (\Exception $e) {
                    $this->logger->error('Deserialization error '. $e->getMessage());
                }
                if ($currentUser->getNickname() == $requestedUser->getNickname()) {
                    return $currentUser;
                }
                return false;
            }
        );

        // если не нашли - добавляем
        $newUserId = false;
        if (!$foundUser) {
            $newUserId = $this->ident->issueId();
            $requestedUser->setId($newUserId);
            $this->storage->append(
                $this->serializer->serialize($requestedUser)
            );
        }

        return $newUserId;
    }
}
