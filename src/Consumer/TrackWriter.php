<?php

namespace App\Consumer;

/**
 * Сохраняет сериализованые объекты Track в файл.
 *
 * @see \App\Entity\Track
 */
class TrackWriter extends JsonWriter
{
}
