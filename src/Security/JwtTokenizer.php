<?php

namespace App\Security;

use Firebase\JWT\JWT;

/**
 * Создание и декодирование JWT токенов.
 */
class JwtTokenizer implements TokenizerInterface
{
    /**
     * Ключ, который подписывает токены
     *
     * @var string
     */
    private $key;

    public function __construct(string $key)
    {
        $this->key = $key;
    }

    /**
     * @{inheritDoc}
     */
    public function encode(array $data): string
    {
        return JWT::encode($data, $this->key, 'HS256');
    }

    /**
     * @{inheritDoc}
     */
    public function decode(string $token): array
    {
        return (array) JWT::decode($token, $this->key, ['HS256']);
    }
}
