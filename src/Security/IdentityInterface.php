<?php

namespace App\Security;

/**
 * Источник уникальных идентификторов.
 */
interface IdentityInterface
{
    /**
     * Создать уникальный идентификтор
     *
     * @return string
     */
    public function issueId(): string;
}
