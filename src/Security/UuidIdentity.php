<?php

namespace App\Security;

use Ramsey\Uuid\Uuid;

/**
 * UUID идентификаторы на базе RFC 4122
 */
class UuidIdentity implements IdentityInterface
{
    /**
     * Уникальный идентификатор узла/хоста
     *
     * @var string
     */
    private $nodeId;

    public function __construct($nodeId)
    {
        $this->nodeId = $nodeId;
    }

    /**
     * @{inheritDoc}
     */
    public function issueId(): string
    {
        return Uuid::uuid1($this->nodeId);
    }
}
