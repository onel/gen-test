<?php

namespace App\Security;

/**
 * Создание и декодирование токенов.
 */
interface TokenizerInterface
{
    /**
     * Создать токен по указанным данным.
     *
     * @param array $data данные
     *
     * @return string токен
     */
    public function encode(array $data): string;

    /**
     * Получить данные по указанному токену.
     *
     * @param string $token токен
     *
     * @return array данные
     */
    public function decode(string $token): array;
}
