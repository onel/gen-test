<?php

namespace App\MessageBus;

/**
 * Интерфейс шины данных приложения.
 */
interface MessageBusInterface
{
    /**
     * Опубликовать сообщение.
     *
     * @param string $channel название канала
     * @param string $msgBody содержимое сообщения
     *
     * @return mixed
     */
    public function publish(string $channel, string $msgBody);

    /**
     * Опубликовать сообщение и ожидать ответа.
     *
     * @param string $channel название канала
     * @param string $msgBody содержимое сообщения
     * @param int $expiration время ожидания
     *
     * @return mixed
     */
    public function request(string $channel, string $msgBody, $expiration = 0);
}
