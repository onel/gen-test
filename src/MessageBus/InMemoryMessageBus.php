<?php

namespace App\MessageBus;

/**
 * Шина данных в памяти
 */
class InMemoryMessageBus implements MessageBusInterface
{
    /**
     * @var array
     */
    protected $published = [];

    /**
     * @var array
     */
    protected $requests = [];

    /**
     * @var array
     */
    protected $responces = [];

    /**
     * @{inheritDoc}
     */
    public function publish(string $channel, string $msgBody)
    {
        $this->published[$channel] = $msgBody;
    }

    /**
     * @{inheritDoc}
     */
    public function request(string $channel, string $msgBody, $expiration = 0)
    {
        var_dump($this->responces);
        $this->requests[$channel] = $msgBody;
        return $this->responces[$channel] ?? false;
    }

    /**
     * @return array
     */
    public function getPublished(): array
    {
        return $this->published;
    }

    /**
     * @param array $published
     *
     * @return InMemoryMessageBus
     */
    public function setPublished(array $published): InMemoryMessageBus
    {
        $this->published = $published;
        return $this;
    }

    /**
     * @return array
     */
    public function getRequests(): array
    {
        return $this->requests;
    }

    /**
     * @param array $requests
     *
     * @return InMemoryMessageBus
     */
    public function setRequests(array $requests): InMemoryMessageBus
    {
        $this->requests = $requests;
        return $this;
    }

    /**
     * @return array
     */
    public function getResponces(): array
    {
        return $this->responces;
    }

    /**
     * @param array $responces
     *
     * @return InMemoryMessageBus
     */
    public function setResponces(array $responces): InMemoryMessageBus
    {
        $this->responces = $responces;
        return $this;
    }
}
