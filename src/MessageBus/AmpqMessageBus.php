<?php

namespace App\MessageBus;

use App\Security\IdentityInterface;
use Psr\Container\ContainerInterface;

/**
 * Реализация шины сообщений на базе AMPQ
 */
class AmpqMessageBus implements MessageBusInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var IdentityInterface
     */
    protected $ident;

    /**
     * AmpqMessageBus constructor.
     *
     * @param ContainerInterface $container
     * @param IdentityInterface $ident
     */
    public function __construct(ContainerInterface $container, IdentityInterface $ident)
    {
        $this->container = $container;
        $this->ident = $ident;
    }

    /**
     * @{inheritDoc}
     */
    public function publish(string $channel, string $msgBody)
    {
        return $this->container
            ->get('old_sound_rabbit_mq.'.$channel.'_producer')
            ->publish($msgBody);
    }

    /**
     * @{inheritDoc}
     */
    public function request(string $channel, string $msgBody, $expiration = 0)
    {
        $requestId = $this->ident->issueId();
        $rpc = $this->container->get('old_sound_rabbit_mq.'.$channel.'_rpc');
        $rpc->addRequest($msgBody, $channel, $requestId, '', $expiration);
        $replies = $rpc->getReplies();
        return $replies[$requestId];
    }
}
