<?php

namespace App\Serializer;

/**
 * Сериализация объектов
 */
interface SerializerInterface
{
    /**
     * Сериализовать в строку.
     *
     * @param $object
     *
     * @return string
     */
    public function serialize($object);

    /**
     * Создать объект из сериализованых данных.
     *
     * @param string $data сериализованные данные
     * @param string $className полное имя класса
     *
     * @return mixed
     */
    public function deserialize(string $data, string $className);
}
