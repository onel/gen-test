<?php

namespace App\Serializer;

use Symfony\Component\Serializer\Serializer as SymfonySerializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Сериализация объектов в json и обратно.
 */
class Serializer implements SerializerInterface
{
    /**
     * @var SymfonySerializer
     */
    private $serializer;

    /**
     * Формат операций сераилации / десериализации
     * @var string
     */
    private $format = 'json';

    /**
     * Создаем стандартный сериализатор.
     */
    public function __construct()
    {
        $this->serializer = new SymfonySerializer(
            [new ObjectNormalizer()],
            [new JsonEncoder()]
        );
    }

    /**
     * @{inheritDoc}
     */
    public function serialize($object)
    {
        return $this->serializer->serialize($object, $this->format);
    }

    /**
     * @{inheritDoc}
     */
    public function deserialize(string $data, string $className)
    {
        return $this->serializer->deserialize($data, $className, $this->format);
    }
}
