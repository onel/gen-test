<?php

namespace App\Controller;

use App\MessageBus\MessageBusInterface;
use App\Serializer\SerializerInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\{Request, Response};
use Symfony\Component\Form\FormInterface;

use App\Entity\{ApiResponse, AuthInfo, User, Track, TrackRequest};
use App\Forms\{AuthForm, RegisterForm, TrackRequestForm};
use App\Security\{IdentityInterface, TokenizerInterface};

/**
 * Сервис авторизации и отслеживания действий пользователя.
 */
class UserController extends Controller
{
    /**
     * @var \App\Security\TokenizerInterface
     */
    private $tokenizer;

    /**
     * @var \App\Security\IdentityInterface
     */
    private $ident;

    /**
     * @var \App\Serializer\SerializerInterface
     */
    private $serializer;

    /**
     * @var MessageBusInterface
     */
    private $msgBus;

    /**
     * Инициализируем сервисы
     *
     * @param TokenizerInterface $tokenizer
     * @param IdentityInterface $ident
     * @param SerializerInterface $serializer
     */
    public function __construct(
        TokenizerInterface $tokenizer,
        IdentityInterface $ident,
        SerializerInterface $serializer,
        MessageBusInterface $msgBus
    ) {
        $this->msgBus = $msgBus;
        $this->tokenizer = $tokenizer;
        $this->ident = $ident;
        $this->serializer = $serializer;
    }

    /**
     * Регистрация пользователя.
     *
     * @Route("/register", methods={"POST"})
     */
    public function register(Request $request)
    {
        $response = new ApiResponse();
        $user = new User();
        $form = $this->createForm(RegisterForm::class, $user);
        $form->submit(json_decode($request->getContent(), true));
        if ($form->isValid()) {
            $newUserId = $this->msgBus->request(
                'register_user',
                $this->serializer->serialize($user),
                10
            );
            if ($newUserId) {
                $response
                    ->setStatus(Response::HTTP_OK)
                    ->setDataKey(ApiResponse::K_TOKEN, $this->tokenizer->encode(['uid' => $newUserId]));
            } else {
                $form->addError(new FormError('Пользователь с таким nickname уже зарегистрирован'));
            }
        }
        if ($response->getStatus() != Response::HTTP_OK) {
            $response
                ->setStatus(Response::HTTP_BAD_REQUEST)
                ->setDataKey(ApiResponse::K_ERRORS, $this->formatErrors($form));
        }
        return $this->apiResponse($response);
    }

    /**
     * Авторизация пользоватя по **nickname** и **password**.
     * Возвращает JSON с профилем пользователя.
     *
     * @Route("/auth", methods={"POST"})
     */
    public function auth(Request $request)
    {
        $response = new ApiResponse();
        $auth = new AuthInfo();
        $form = $this->createForm(AuthForm::class, $auth);
        $form->submit(json_decode($request->getContent(), true));
        if ($form->isValid()) {
            $replyData = $this->msgBus->request(
                'auth_info',
                $this->serializer->serialize($auth),
                5
            );
            if ($replyData) {
                $user = $this->serializer->deserialize($replyData, User::class);
                $response
                    ->setStatus(Response::HTTP_OK)
                    ->setDataKey(
                        ApiResponse::K_TOKEN,
                        $this->tokenizer->encode(['uid' => $user->getId()])
                    );
            } else {
                $response->setStatus(Response::HTTP_UNAUTHORIZED);
            }
        } else {
            $response->setStatus(Response::HTTP_BAD_REQUEST);
        }
        if ($response->getStatus() != Response::HTTP_OK) {
            $response->setDataKey(ApiResponse::K_ERRORS, $this->formatErrors($form));
        }
        return $this->apiResponse($response);
    }

    /**
     * Отслеживание действий пользователя.
     * Для не зарегистрированых пользователей - создает новый ID.
     *
     * @Route("/track", methods={"POST"})
     */
    public function track(Request $request)
    {
        $response = new ApiResponse();
        $track = new TrackRequest();
        $form = $this->createForm(TrackRequestForm::class, $track);
        $form->submit(json_decode($request->getContent(), true));
        if ($form->isValid()) {
            $decoded = [];
            if ($token = $track->getToken()) {
                $decoded = $this->tokenizer->decode($token);
            }
            $userId = $decoded['uid'] ?? $this->ident->issueId();
            $this->msgBus->publish(
                'save_track',
                $this->serializer->serialize(
                    (new Track())
                        ->setId($this->ident->issueId())
                        ->setSourceLabel($track->getSourceLabel())
                        ->setIdUser($userId)
                        ->setDateCreated('Y-m-d H:i:s')
                )
            );
            $response
                ->setStatus(Response::HTTP_OK)
                ->setDataKey(
                    ApiResponse::K_TOKEN,
                    $track->getToken() ?? $this->tokenizer->encode(['uid' => $userId])
                );
        } else {
            $response
                ->setStatus(Response::HTTP_BAD_REQUEST)
                ->setDataKey(ApiResponse::K_ERRORS, $this->formatErrors($form));
        }
        return $this->apiResponse($response);
    }

    /**
     * Оформить список ошибок формы в виде массива.
     *
     * @param FormInterface $form
     *
     * @return array
     */
    protected function formatErrors(FormInterface $form): array
    {
        $errors = [];
        foreach ($form->getErrors(true, true) as $i) {
            $errors[] = [
                'field' => $i->getOrigin()->getName(),
                'message' => $i->getMessage(),
            ];
        }
        return $errors;
    }

    /**
     * Сформировать стандартный ответ сервера
     *
     * @param ApiResponse $i
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    protected function apiResponse(ApiResponse $i)
    {
        return $this->json($i->getData(), $i->getStatus());
    }
}
