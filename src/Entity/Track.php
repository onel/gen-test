<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Объект, представляющий взаимодействие пользователя со страницей.
 */
class Track
{
    /**
     * Идентификатор взаимодействия.
     *
     * @var string
     */
    private $id;

    /**
     * Идентификатор пользователя.
     *
     * @see \App\Entity\User
     * @var string
     */
    private $id_user;

    /**
     * Параметр/метка взаимодействия.
     *
     * @var string
     * @Assert\NotBlank()
     */
    private $source_label;

    /**
     * Дата создания, в формате "YYYY-mm-dd H:i:s".
     *
     * @var string
     */
    private $date_created;

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return Track
     */
    public function setId(string $id): Track
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdUser(): ?string
    {
        return $this->id_user;
    }

    /**
     * @param string $id_user
     *
     * @return Track
     */
    public function setIdUser(string $id_user): Track
    {
        $this->id_user = $id_user;
        return $this;
    }

    /**
     * @return string
     */
    public function getSourceLabel(): ?string
    {
        return $this->source_label;
    }

    /**
     * @param string $source_label
     *
     * @return Track
     */
    public function setSourceLabel(string $source_label): Track
    {
        $this->source_label = $source_label;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateCreated(): ?string
    {
        return $this->date_created;
    }

    /**
     * @param string $date_created
     *
     * @return Track
     */
    public function setDateCreated(string $date_created): Track
    {
        $this->date_created = $date_created;
        return $this;
    }
}
