<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Запрос на отслеживание действия
 */
class TrackRequest
{
    /**
     * Параметр/метка взаимодействия.
     *
     * @var string
     * @Assert\NotBlank()
     */
    private $source_label;

    /**
     * JWT токен пользователя.
     *
     * @var string
     */
    private $token;

    /**
     * @return string
     */
    public function getSourceLabel(): ?string
    {
        return $this->source_label;
    }

    /**
     * @param string $source_label
     *
     * @return TrackRequest
     */
    public function setSourceLabel(string $source_label): TrackRequest
    {
        $this->source_label = $source_label;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string $token
     *
     * @return TrackRequest
     */
    public function setToken(string $token): TrackRequest
    {
        $this->token = $token;
        return $this;
    }
}
