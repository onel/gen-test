<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Пользователь системы.
 */
class User
{
    /**
     * UUID пользователя.
     *
     * @var string
     */
    private $id;

    /**
     * Имя.
     *
     * @var string
     * @Assert\NotBlank()
     */
    private $firstname;

    /**
     * Фамилия.
     *
     * @var string
     * @Assert\NotBlank()
     */
    private $lastname;

    /**
     * Никнейм.
     *
     * @var string
     * @Assert\NotBlank()
     */
    private $nickname;

    /**
     * Возраст.
     *
     * @var string
     * @Assert\NotBlank()
     */
    private $age;

    /**
     * Пароль.
     *
     * @var string
     * @Assert\NotBlank()
     */
    private $password;

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return User
     */
    public function setId(?string $id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname(?string $firstname): User
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname(?string $lastname): User
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return string
     */
    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     *
     * @return User
     */
    public function setNickname(?string $nickname): User
    {
        $this->nickname = $nickname;
        return $this;
    }

    /**
     * @return string
     */
    public function getAge(): ?string
    {
        return $this->age;
    }

    /**
     * @param string $age
     *
     * @return User
     */
    public function setAge(?string $age): User
    {
        $this->age = $age;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return User
     */
    public function setPassword(?string $password): User
    {
        $this->password = $password;
        return $this;
    }
}
