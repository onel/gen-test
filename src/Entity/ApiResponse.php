<?php

namespace App\Entity;

/**
 * Ответ api сервера
 */
class ApiResponse
{
    const K_ERRORS = 'errors';
    const K_TOKEN = 'token';

    /**
     * @var integer
     */
    private $status;

    /**
     * @var array
     */
    private $data;

    /**
     * ApiResponse constructor.
     */
    public function __construct()
    {
        $this->data = [
            'token' => '',
            'errors' => [],
        ];
    }

    /**
     * @return int
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return ApiResponse
     */
    public function setStatus(int $status): ApiResponse
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     *
     * @return ApiResponse
     */
    public function setData(array $data): ApiResponse
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Добавить значение в ответ
     *
     * @param string $key ключ
     * @param mixed $value значение
     *
     * @return ApiResponse
     */
    public function setDataKey(string $key, $value): ApiResponse
    {
        $this->data[$key] = $value;
        return $this;
    }
}
