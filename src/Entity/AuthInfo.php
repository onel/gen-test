<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Auth
 */
class AuthInfo
{
    /**
     * Ник.
     *
     * @var string
     * @Assert\NotBlank()
     */
    private $nickname;

    /**
     * Пароль.
     *
     * @var string
     * @Assert\NotBlank()
     */
    private $password;

    /**
     * @return string
     */
    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     *
     * @return AuthInfo
     */
    public function setNickname(string $nickname): AuthInfo
    {
        $this->nickname = $nickname;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return AuthInfo
     */
    public function setPassword(string $password): AuthInfo
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Запрос авторизации идентичен?
     *
     * @param AuthInfo $compareWith с чем сравниваем
     *
     * @return bool
     */
    public function isEqual(AuthInfo $compareWith): bool
    {
        return (
            $this->getNickname() == $compareWith->getNickname()
            && $this->getPassword() == $compareWith->getPassword()
        );
    }
}
