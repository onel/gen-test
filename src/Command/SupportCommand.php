<?php

namespace App\Command;

use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SupportCommand
 * @package App\Command
 */
class SupportCommand extends Command
{
    private $container;

    public function __construct($name = null, ContainerInterface $container)
    {
        $this->container = $container;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setName('app:support')
            ->setDescription('Создает необходимые файлы для работы воркеров');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $storageDir = $this->container->getParameter('storage_dir');
        $pathList = [
            $storageDir.'/'.$this->container->getParameter('storage_file_users'),
            $storageDir.'/'.$this->container->getParameter('storage_file_track')
        ];
        foreach ($pathList as $path) {
            if (!file_exists($path)) {
                $output->writeln('Create: '.$path);
                touch($path);
            } else {
                $output->writeln('Alredy exist: '.$path);
            }
        }
    }
}
