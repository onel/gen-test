FROM alpine:3.6

RUN apk add --update \
    php7-apcu \
    php7-ctype \
    php7-curl \
    php7-dom \
    php7-gd \
    php7-iconv \
    php7-imagick \
    php7-json \
    php7-intl \
    php7-mcrypt \
    php7-mbstring \
    php7-opcache \
    php7-openssl \
    php7-xml \
    php7-zlib \
    php7-phar \
    php7-tokenizer \
    php7-session \
    php7-simplexml \
    php7-bcmath \
    make \
    curl \
    supervisor

RUN rm -rf /var/cache/apk/* && rm -rf /tmp/*

ADD docker/php/app.ini /etc/php7/cli/conf.d/

WORKDIR /var/www/app

COPY ./.env.dist ./.env

RUN curl -sS https://getcomposer.org/installer | \
    php -- --install-dir=/usr/bin/ --filename=composer
COPY composer.json ./
COPY composer.lock ./
RUN composer install --no-scripts --no-autoloader
COPY . ./
RUN composer dump-autoload --optimize && \
	composer run-script post-install-cmd && \
	php bin/console app:support

RUN mkdir -p /var/log/supervisor
COPY docker/workers/supervisord.conf /etc/supervisord.conf
CMD ["supervisord", "-n", "-c", "/etc/supervisord.conf"]

EXPOSE 9000