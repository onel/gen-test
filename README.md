# gen-test

Сервис авторизации и отслеживания действий пользователя. 

## Документация

[В формет swagger](https://app.swaggerhub.com/apis/hcbogdan/gen-test/1.0.0) сформирована по *docs/swagger.yaml*

## Как развернуть

```bash
$ git clone https://bitbucket.org/onel/gen-test \
    && cd gen-test \ 
    && docker-compose up
```

Проверяем:

```bash
curl -i --request POST 'http://127.0.0.1:9001/track' --data '{"source_label": "1"}'
curl -i --request POST 'http://127.0.0.1:9001/auth' --data '{"nickname": "1", "password": "1"}'
curl -i --request POST 'http://127.0.0.1:9001/register' --data '{"firstname": "1", "lastname": "1", "nickname": "1", "age": "1", "password": "1"}'
```

Если нужно запустить воркеров вне контейнера:

```bash
php bin/console rabbitmq:rpc-server -vvv register_user & \
php bin/console rabbitmq:rpc-server -vvv auth_info & \
php bin/console rabbitmq:consumer -vvv task_track 
```

## Особенности

+ Обрабатываемые JSON файлы имеют ограничения: каждая новая запись в файле - с новой строки.

+ Юнит тесты должны проводиться с запущенными воркерами, так как не нашел способа замены сервиса (которому передают параметры в runtime) при тестировании контроллера.
