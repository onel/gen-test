<?php

namespace App\Tests\Controller;

use App\MessageBus\InMemoryMessageBus;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Тесты требуют рабочего окружения (запущенных воркеров)
 * Т.к не нашел метода, с помощью которого можно заменить
 * сервис MessageBusInterface, с указанием параметров
 */
class UserControllerTest extends WebTestCase
{
    /**
     * Не корректный запрос авторизации
     */
    public function testAuthInvalid()
    {
        $requestData = [
            'nickname' => '',
            'password' => '',
        ];
        $client = static::createClient();
        $client->request('POST', '/auth', [], [], [], json_encode($requestData));
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
    }

    /**
     * Не корректный запрос регистрации
     */
    public function testRegisterInvalid()
    {
        $requestData = [
            'nickname' => '',
            'password' => '',
        ];
        $client = static::createClient();
        $client->request('POST', '/register', [], [], [], json_encode($requestData));
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
    }

    /**
     * Не корректный запрос отслеживания
     */
    public function testTrackInvalid()
    {
        $requestData = [
            'source_label' => '',
        ];
        $client = static::createClient();
        $client->request('POST', '/track', [], [], [], json_encode($requestData));
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
    }

    /**
     * Сгенерировать данные для создания профиля пользователя
     */
    protected function generateUserForm()
    {
        $uniqId = uniqid('user');
        return [
            'firstname' => $uniqId,
            'lastname' => $uniqId,
            'nickname' => $uniqId,
            'age' => $uniqId,
            'password' => $uniqId,
        ];
    }

    /**
     * Проверить наличие токена в сериализованной строке.
     *
     * @param string $content строка, в которой JSON
     */
    protected function assertContentWithToken(string $content)
    {
        $this->assertNotEmpty($content);
        $decoded = json_decode($content, true);
        $this->assertArrayHasKey('token', $decoded);
        $this->assertNotEmpty($decoded['token']);
    }

    /**
     * Регистрация нового пользователя + авторизация
     */
    public function testRegisterAndAuth()
    {
        $requestData = $this->generateUserForm();
        $client = static::createClient();
        $client->request('POST', '/register', [], [], [], json_encode($requestData));
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertContentWithToken($client->getResponse()->getContent());

        $requestData = [
            'nickname' => $requestData['nickname'],
            'password' => $requestData['password'],
        ];
        $client = static::createClient();
        $client->request('POST', '/auth', [], [], [], json_encode($requestData));
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertContentWithToken($client->getResponse()->getContent());
    }

    /**
     * Попытка повторной регистрации по идентичным данным
     */
    public function testRepeatedRegister()
    {
        $requestData = $this->generateUserForm();
        $client = static::createClient();
        $client->request('POST', '/register', [], [], [], json_encode($requestData));
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        $client = static::createClient();
        $client->request('POST', '/register', [], [], [], json_encode($requestData));
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
    }

    /**
     * Тест отслеживания
     */
    public function testTrack()
    {
        $requestData = [
            'source_label' => 'test',
        ];
        $client = static::createClient();
        $client->request('POST', '/track', [], [], [], json_encode($requestData));
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertContentWithToken($client->getResponse()->getContent());
    }
}
