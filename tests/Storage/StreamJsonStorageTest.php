<?php
namespace App\Tests\Storage;

use App\Storage\StreamJsonStorage;
use PHPUnit\Framework\TestCase;

class StreamJsonStorageTest extends TestCase
{
    public function testStorage()
    {
        $filePath = '/tmp/test.json';
        $data = ['id' => 1];
        $dataRow = json_encode($data);
        $storage = new StreamJsonStorage($filePath, 'w+');
        $storage->append($dataRow);
        $foundRow = $storage->find(function($line) use ($dataRow) {
            if ($line == $dataRow) {
                return $line;
            }
        });
        $this->assertEquals($dataRow, $foundRow);
        unlink($filePath);
    }

}