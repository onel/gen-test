<?php

namespace App\Tests\Security;

use App\Security\JwtTokenizer;
use PHPUnit\Framework\TestCase;

class JwtTokenizerTest extends TestCase
{
    public function testEncodeDecode()
    {
        $key = '1';
        $data = [
            'test' => 'data'
        ];
        $tokenizer = new JwtTokenizer($key);

        $encoded = $tokenizer->encode($data);
        $this->assertNotEmpty($encoded);

        $decoded = $tokenizer->decode($encoded);
        $this->assertEquals($data, $decoded);
    }
}